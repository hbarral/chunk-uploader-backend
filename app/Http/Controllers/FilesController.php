<?php

namespace App\Http\Controllers;

use App\Helpers\Mimes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['status' => 'ok'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'is_last' => [
                'required',
                function ($attribute, $value, $fail) {
                    if ($value === false || $value === true) {
                        $fail($attribute.' must be true or false');
                    }
                },
            ],
            'file' => [
                'required',
                function ($attribute, $value, $fail) {
                    $mime = $value->getClientMimeType();
                    $mimesAllowed = Mimes::getMimes();
                    if (!in_array(
                        $mime,
                        $mimesAllowed
                    )) {
                        $fail('Illegal file type');
                    }
                },

            ]
        ])->validate();


        // simulation of errors
        $value = $this->checkWithProbability(95/100);

        if ($value) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $path = Storage::disk('local')->path("chunks/{$filename}");

            File::append($path, $file->get());

            if ($request->has('is_last') && $request->boolean('is_last')) {
                $name = basename($path, '.part');

                File::move($path, public_path($name));
            }

            $fileToDelete = public_path(basename($filename, '.part'));
            File::delete($fileToDelete);

            return response()->json(['uploaded' => true]);
        } else {
            return response()->json(['uploaded' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function checkWithProbability($probability=0.1, $length=10000)
    {
        $test = mt_rand(1, $length);
        return $test<=$probability*$length;
    }
}
